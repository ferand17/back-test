package com.nabenik.repository;


import com.nabenik.model.Actor;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

import java.util.List;

@RequestScoped
@Transactional(Transactional.TxType.REQUIRED)
public class ActorRepository {

    @Inject
    EntityManager em;

    public Actor findById(Long id){
        return em.find(Actor.class, id);
    }

    public Actor create(Actor actor){
        em.persist(actor);
        return actor;
    }

    public Actor update(Actor actor){
        return em.merge(actor);
    }

    public void delete(Actor actor){
        em.remove(actor);
    }

    public List<Actor> listAll(){
        return em.createQuery("select a from Actor a",Actor.class).getResultList();
    }

}
