package com.nabenik.repository;


import com.nabenik.model.Movie;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

import java.util.List;

@RequestScoped
@Transactional(Transactional.TxType.REQUIRED)
public class MovieRepository {

    @Inject
    EntityManager em;

    public Movie findById(Long id){
        return em.find(Movie.class, id);
    }

    public Movie create(Movie movie){
        em.persist(movie);
        return movie;
    }

    public Movie update(Movie movie){
        return em.merge(movie);
    }

    public void delete(Movie movie){
        em.remove(movie);
    }

    public List<Movie> listAll(){
        return em.createQuery("select m from Movie m",Movie.class).getResultList();
    }

}
