package com.nabenik.controller;

import com.nabenik.model.Movie;
import com.nabenik.repository.MovieRepository;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.net.URI;
import java.util.List;

@Path("/movies")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Transactional
public class MovieController {


    MovieRepository movieRepository;


    @Inject
    public MovieController(MovieRepository movieRepository){
        this.movieRepository=movieRepository;
    }

    @GET
    public Response listAll(){
        return Response.ok(movieRepository.listAll()).header("Access-Control-Allow-Origin", "*").build();
    }

    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") Long id){
        return Response.ok(movieRepository.findById(id)).header("Access-Control-Allow-Origin", "*").build();
    }

    @POST
    public Response create(Movie movie){
        movieRepository.create(movie);
        return Response.created(URI.create("/movies/"+movie.getMovieId()))
                .header("Access-Control-Allow-Origin", "*")
                .build();
    }

    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") Long id, Movie movie){
        movieRepository.update(movie);
        return Response.ok(movie).header("Access-Control-Allow-Origin", "*").build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id){
        Movie movie = movieRepository.findById(id);
        if(movie == null){
            return Response.status(Response.Status.NOT_FOUND).header("Access-Control-Allow-Origin", "*").build();
        }else{
            movieRepository.delete(movie);
            return Response.ok().header("Access-Control-Allow-Origin", "*").build();
        }
    }
}
