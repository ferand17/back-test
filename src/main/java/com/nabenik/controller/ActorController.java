package com.nabenik.controller;

import com.nabenik.model.Actor;
import com.nabenik.repository.ActorRepository;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.net.URI;
import java.util.List;
@Path("/actor")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Transactional
public class ActorController {


    ActorRepository actorRepository;


    @Inject
    public ActorController(ActorRepository actorRepository){
        this.actorRepository=actorRepository;
    }

    @GET
    public Response listAll(){
        return Response.ok(actorRepository.listAll()).header("Access-Control-Allow-Origin", "*").build();
    }

    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") Long id){
        return Response.ok(actorRepository.findById(id)).header("Access-Control-Allow-Origin", "*").build();
    }

    @POST
    public Response create(Actor actor){
        actorRepository.create(actor);
        return Response.created(URI.create("/actor/"+actor.getActorId())).header("Access-Control-Reques", "*").build();
    }

    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") Long id, Actor actor){
        actorRepository.update(actor);
        return Response.ok(actor).header("Access-Control-Allow-Origin", "*").build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id){
        Actor actor = actorRepository.findById(id);
        if(actor == null){
            return Response.status(Response.Status.NOT_FOUND).header("Access-Control-Allow-Origin", "*").build();
        }else{
            actorRepository.delete(actor);
            return Response.ok().header("Access-Control-Allow-Origin", "*").build();
        }
    }
}
