package com.nabenik.rest;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

/**
 * Configures a JAX-RS endpoint. Delete this class, if you are not exposing
 * JAX-RS resources in your application.
 *
 * @author tuxtor
 */
@ApplicationPath("/rest")
public class JAXRSConfiguration extends Application {

}
