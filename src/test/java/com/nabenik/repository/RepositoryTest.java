package com.nabenik.repository;

import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.util.EntityManagerProducer;
import jakarta.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit5.ArquillianExtension;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(ArquillianExtension.class)
public class RepositoryTest {
    @Inject
    MovieRepository movieRepository;

    @Inject
    ActorRepository actorRepository;

    @Deployment
    public static WebArchive createDeployment(){
        WebArchive war = ShrinkWrap.create(WebArchive.class)
                .addClass(MovieRepository.class)
                .addClass(Movie.class)
                .addClass(Actor.class)
                .addClass(ActorRepository.class)
                .addClass(EntityManagerProducer.class)
                .addAsResource("persistence.xml","META-INF/persistence.xml")
                .addAsWebInfResource("glassfish-resources.xml","glassfish-resources.xml");
        System.out.println(war.toString(true));
        return war;
    }

    @Test
    public void allPersistence(){
        // create movie
        Movie movie = new Movie("El silencio de Jimmy", "2014", "4 años");
        Movie movie2 = new Movie("FreeGuy", "2021", "2 hr");
        movieRepository.create(movie);
        movieRepository.create(movie2);
        System.out.println("->CreateMovieTest->Movie Id " + movie.getMovieId());
        assertNotNull(movie.getMovieId());
        //findById movie
        Movie rdbmsMovie = movieRepository.findById(movie.getMovieId());
        assertEquals(movie.getTitle(),rdbmsMovie.getTitle());
        System.out.println("->FindByIDMovieTest->rdbmsMovie Id " + rdbmsMovie.getMovieId());
        //actor created
        Actor actor = new Actor();
        actor.setCountry("US");
        actor.setBirthday("NONE");
        actor.setName("Ryan Reynolds");
        actor.setMovie(movie2);
        actorRepository.create(actor);
        System.out.println("->CreateActorTest->Actor Id " + actor.getActorId());
        assertNotNull(actor.getActorId());
        //FindByID Actor
        Actor rdbmsActor = actorRepository.findById(actor.getActorId());
        assertEquals(actor.getName(),rdbmsActor.getName());
        System.out.println("->FindByIDActorTest->rdbmsActor Id " + rdbmsActor.getActorId());
        //Update movie
        movie.setTitle("Avengers");
        movieRepository.update(movie);
        rdbmsMovie = movieRepository.findById(movie.getMovieId());
        assertEquals(movie.getTitle(),rdbmsMovie.getTitle());
        System.out.println("->UpdateMovieTest->Movie title " + movie.getTitle());
        //Update Actor
        actor.setName("Ryan Reynolds JR");
        actorRepository.update(actor);
        rdbmsActor = actorRepository.findById(actor.getActorId());
        assertEquals(actor.getName(),rdbmsActor.getName());
        System.out.println("->UpdateActorTest->rdbmsActor Name " + rdbmsActor.getName());
        //Delete Movie
        movieRepository.delete(movie);
        rdbmsMovie = movieRepository.findById(movie.getMovieId());
        assertNull(rdbmsMovie);
        System.out.println("->DeleteMovieTest->Delete");
        //Delete Actor
        actorRepository.delete(actor);
        rdbmsActor = actorRepository.findById(actor.getActorId());
        assertNull(rdbmsActor);
        System.out.println("->DeleteActorTest->Delete");
        System.out.println("All Tests Passed");
    }
}
