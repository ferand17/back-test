# Nabenik's Enterprise Java basic test

Hi and welcome to this test. As many technical interviews, main test objective is to establish your actual Enterprise coding skills, being:

* Java knowledge
* DDD knowledge
* General toolkits, SDK's and other usages
* Jakarta EE general skills

To complete this test, please create a fork of this repository, fix the source code if required, add your answers to this readme file, commit the solutions/answers to YOUR copy and create a pull request to the original repo.

This document is structured using [GitHub Markdown Flavor](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code).

## General questions

1. How to answer these questions?

> Like this

2. Please describe briefly the main purpose for the following Jakarta EE specs, also add in your answer http links to actual implementations. Is this project using all specs?

- EJB
    >  is used to execute business tasks for a client on the application server
- Servlet
   > is used to extend the capabilities of a server as they can respond to any 
  > type of request
- CDI
   > they are classes in java that with annotations I can manage their control 
  > since their components are more generic (USE IN PROJECT)
- JAX-RS
   > is a specification for creating rest api that relies on annotations to 
  > simplify client development and deployment (USE IN PROJECT)

3. Which of the following is an application server?

* Open Liberty
* Apache TomEE
* Eclipse Jetty
* Eclipse Glassfish
* Oracle Weblogic
   > Apache TomEE and Eclipse Glassfish

4. In your opinion, what's the main benefit of moving from Java 11 to Java 17?
    > in principle it is the latest update of java lts means that it will have 
   > a lot of maintenance, but leaving that aside you can see a notable change 
   > in syntax in native operations such as the switch and boolean expressions 
   > and not only its minimization in the code but also its optimization
5. Is it possible to run this project (as is) over Java 17? Why?
    > yes it could since java 17 supports jakarta 9
6. Is it possible to run this project with GraalVM Native? Why?
    > I'm not sure but if the syntax in the structure of pow.xml is changed it
   > may be because both maven and graalvm support jakarta ee and java ee
7. How do you run this project directly from CLI without configuring any application server? Why is it possible?
    > it could be used without a server client if you configure an instance that deploys itself the application
   > as an http listener because when you activate it after the build it deploys itself, like using a jar generated 
   > with springboot
## Development tasks

To solve this questions please use Gitflow workflow, still, your answers should be in the current branch.

Please also include screenshots on every task. You don't need to execute every task to submit your pull request but feel free to do it :).

0. (easy) Show your terminal demonstrating your installation of OpenJDK 11
   >![img.png](ImgReadme/dot0.png)
1. (easy) Run this project using an IDE/Editor of your choice
   >![img.png](ImgReadme/dot1.png)
2. (medium) Execute the movie endpoint operations using curl, if needed please also check/fix the code to be REST compliant
   >![img.png](ImgReadme/dot2.png)
3. (medium) Write an SPA application using Angular, the application should be a basic CRUD that uses Movie operations, upload this application to a new Bitbucket repo and include the link as answer
    > [https://bitbucket.org/ferand17/front-test-nabenik/](https://bitbucket.org/ferand17/front-test-nabenik/)
   > [![Alt text](https://img.youtube.com/vi/9LrY5f0jM0E/0.jpg)](https://www.youtube.com/watch?v=9LrY5f0jM0E)
4. (medium) This project has been created using Java EE APIs, please move it to Jakarta EE APIs and switch it to a compatible implementation (if needed)
    >![img.png](ImgReadme/dot4.png)
5. (hard) Please identify the Integration Test for `MovieRepository`, after that implement each of the non-included CRUD methods
    >![img.png](ImgReadme/dot5.png)
6. (hard) Please write the Repository and Controller for Actor model, after that implement each of the CRUD methods using an Integration Test
    >![img.png](ImgReadme/dot6.png)
7. (hard) This project uses vanilla Java EE for Database Persistence, please integrate Testcontainers with MySQL for testing purposes
    >![img.png](ImgReadme/dot7.png)
8. (nightmare) This source code includes only Java EE APIs, hence it's possible to port it to [Open Liberty](https://openliberty.io/). Do it and don't port Integration Tests 
    >
